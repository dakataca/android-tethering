#!/bin/bash
# 🌟🌟🌟 Script para administrar interfaces de red Android Tethering en Archilnux 🌟🌟🌟
# 📂 /usr/bin/tethering
# 📦 packages: https://archlinux.org/packages/?name=usb_modeswitch
# Dependencias: usb_modeswitch


# 🌟🌟 Funsiones 🌟🌟


validate (){
	local -r regex="$1"
	local -r ext="$2"
	declare exist="false" vendor="" model="" vendorIdFile="" modelIdFile=""

	if [ "$ext" == ".link" ] || [ "$ext" == ".rules" ]; then
		declare -r vendorId="$3" modelId="$4"
		[ "$ext" == ".link" ] && vendor="VENDOR" model="MODEL" || vendor="Vendor" model="Model"
		for file in $(ls $regex); do
			vendorIdFile="$(get_id_file "$file" "$vendor")"
			modelIdFile="$(get_id_file "$file" "$model")"
			if [ "$vendorId" == "$vendorIdFile" ] && [ "$modelId" == "$modelIdFile" ]; then
                                [ "$exist" == "true" ] && echo "Eliminando fichero $ext... $file.$(rm -f "$file")"
                                exist="true"
                        fi
		done
	elif [ "$ext" == ".network" ]; then
		declare -r interfacesLink="$(get_interfaces "/etc/systemd/network/10-net[0-9]*.link")" \
			interfacesRules="$(get_interfaces "/etc/udev/rules.d/90-tethering-net[0-9]*.rules")"
		declare intfLink="" intf=""

		if [ "$interfacesLink" == "$interfacesRules" ]; then
			for fileLink in $(ls /etc/systemd/network/10-net[0-9]*.link); do
				intfLink="$(get_id_file "$fileLink" "net")"
				exist="false"
				for file in $(ls $regex); do
					intf="$(get_id_file "$file" "net")"	
					if [ "$intfLink" == "$intf" ]; then
						[ "$exist" == "true" ] && rm -f $file
						exist="true"
					fi
				done
			done
		else
			show_error "ficheros LINK \"$interfacesLink\" distintos de ficheros RULES \"$interfacesRules\"."
		fi
	else
		show_error "extensión \"$ext\" errada al validar ficheros duplicados."
	fi
	echo "$exist"
}

validate_files (){
	declare -r info="$1" regex="$2" vendorId="$3" modelId="$4"
	declare -r ext="$(get_extension "$regex")" interface="$(get_interface "$regex")"
	declare -ir numFiles=$(count_files "$regex")
	declare -i index=-1
	declare exist=""

	if (( numFiles >= 1 )); then
		if [ "$ext" == ".link" ] || [ "$ext" == ".rules" ]; then
			exist=$(validate "$regex" "$ext" "$vendorId" "$modelId")
		elif [ "$ext" == ".network" ]; then
			exist=$(validate "$regex" "$ext")
		else
			show_error "extensión <$ext> errada al validar ficheros."
		fi
	else
		exist="false"
	fi

	if [ "$exist" == "false" ]; then
		index=$(get_index "$regex" "$ext")
		create_file $index "$info" "$interface" "$ext" "$vendorId" "$modelId"
	else
		echo "Fichero ya existe."
	fi
}

# 🌟 Module error.
source "$(dirname "$0")/android-tethering-modules/show_error"

# Comprobar si el usuario es root
if [ "$EUID" -ne 0 ]; then
	show_error "no puede realizar esta operación, a menos que sea superusuario."
	exit 1
fi

# 🌟 Modules
source "$(dirname "$0")/android-tethering-modules/get_info"
source "$(dirname "$0")/android-tethering-modules/numeric"
source "$(dirname "$0")/android-tethering-modules/create_files"
source "$(dirname "$0")/android-tethering-modules/dns_server_dnssec"


# 🌟 Variables globales

# Get nombre predeterminado o modificado de la interface de red Android Tethering.
declare -gr ACTUAL_INTERFACE=$(ls /sys/class/net/ | grep -E "enp[0-9]s[0-9]{2}([a-z][0-9]){2}|net[0-9]+" || show_error "al obtener actual interface." $?) INTERFACE="net"


# 🌟🌟 Main 🌟🌟

# Actual interface  no está vacía?
if [ ! "$ACTUAL_INTERFACE" == "" ]; then

        # Get información (ID's) de la interface actual del dispositivo android conectado.
	readonly INFO=$(udevadm info /sys/class/net/$ACTUAL_INTERFACE | grep -oE "ID_(VENDOR|MODEL)(_ID){,1}=[0-9a-zA-Z]{4,}" || \
		show_error "al obtener información del dispositivo tethering.")

	declare -r VENDOR_ID=$(get_id "ID_VENDOR_ID=" "$INFO" || show_error "get VENDOR_ID.") \
		MODEL_ID=$(get_id "ID_MODEL_ID=" "$INFO" || show_error "get VENDOR_ID.")

	validate_files "$INFO" "/etc/systemd/network/10-$INTERFACE[0-9]*.link" "$VENDOR_ID" "$MODEL_ID"
	validate_files "$INFO" "/etc/udev/rules.d/90-tethering-$INTERFACE[0-9]*.rules" "$VENDOR_ID" "$MODEL_ID"
	validate_files "$INFO" "/etc/systemd/network/50-$INTERFACE[0-9]*.network"
	
else
        echo "Ningún dispositivo Android Tetherin disponible, \
asegúrese de que su dispositivo Android está conectado y el modo Tethering Modem USB está activo."
fi
