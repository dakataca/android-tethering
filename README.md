[TOC]

# Android-tethering con Systemd-Networkd

> **Nota**: He creado [una guía](https://gitlab.com/dakataca/guides/-/blob/main/Android%20Tethering.md?ref_type=heads#android-tethering) para los que prefieren hacerlo manualmente.

El `script` `bash` se encarga de validar o crear tres ficheros de configuración necesarios para compartir el Internet de su Smartphone Android a su Computadora vía cable usb cuando usa el administrador de red completo [systemd-networkd](https://wiki.archlinux.org/title/systemd-networkd#Wired_and_wireless_adapters_on_the_same_machine), es decir, [Android Tethering](https://wiki.archlinux.org/title/android_tethering). El `script` es capaz de eliminar ficheros repetidos y validar los ya existentes siempre y cuando compartan las [recomendaciones de la wiki de Archlinux](https://wiki.archlinux.org/title/Network_configuration#Change_interface_name) para cambiar nombre de interfaces de red.

# Ficheros de configuración

Cada nombre de fichero termina con un índice generado automáticamente, para que cuando se conecten dispositivos Android Tethering distintos, aumente en uno el índice de cada fichero. Por ejemplo, si es el primer dispositivo Android que se conecta automáticamente iniciará con el índice cero (`net0`):

1. `/etc/systemd/network/10-net0.link`: Cambiar el nombre predeterminado de la red para el dispositivo Android específico.
2. `/etc/udev/rules.d/90-tethering-net0.rules`: Automáticamente usar el teléfono como puerta de enlace.
3. `/etc/systemd/network/50-net0.network`: Configuración de la red tethering en `Systemd-Networkd`.

El próximo dispositivo Android que se conecte tendría el índice uno (`1`), el siguiente el dos (`2`) y así sucesivamente. En caso de que el dispositivo ya haya sido configurado previamente, dirá que los ficheros ya existen.

# Validar configuración cableada e inalámbrica de Systemd-networkd

Si tiene configuradas correctamente tanto la red cableada como la inalámbrica en `systemd-networkd` debería tener inicialmente solo los ficheros de cada implementación:

```shell
ls /etc/{systemd/network,udev/rules.d/}
```

![](imgs/systemd-networkd.png)

# Instalación

## Archlinux

Usando `yay`:

```shell
yay -S android-tethering
```

Usando `paru`:

```shell
paru -S android-tethering
```



# Uso

```shell
android-tethering
```

## Validar

Permisos de superususuario:

![](imgs/permisons_sudo.png)

Dispositivo Android Tethering:![](imgs/desconectado.png)

Conecte el cable usb entre su computadora y teléfono Android y active el modo Modem USB.

## Interface de red por defecto

Listar interface de red Android Tethering por defecto:

```shell
ls /sys/class/net/ | grep -E "enp[0-9]s[0-9]{2}([a-z][0-9]){2}|net[0-9]+"
```

![](imgs/actual_interface.png)

## Ejecutar `script`

```shell
android-tethering
```

![](imgs/execute.png)

> **Podemos apreciar que:**
>
> - La interface Android Tethering por defecto es `enp0s20u3u3`.
> - Se crearon correctamente los tres ficheros de configuración.
> - Se cargaron correctamente las interfaces de red cableada (`enp0s25`) e inalámbrica (`wlan0`).

## Listar ficheros creados:

```shell
ls /etc/{systemd/network,udev/rules.d/}
```

![](imgs/list_files.png)

## Características de la nueva red Android Tethering creada (tenga en cuenta el índice generado)

```shell
ip a show net0
```

![](imgs/tethering_show.png)

# Verificar conexión

Comprobar conexión de todas las interfaces de red habilitadas.

```shell
ls /sys/class/net
ping -c3 -I enp0s25 archlinux.org
ping -c3 -I wlan0 archlinux.org
ping -c3 -I net0 archlinux.org
```

![](imgs/interfaces.png)

Si se ejecuta el `script` por segunda ves con el mismo dispositivo Android Tethering conectado imprime:
![](imgs/exist_file.png)